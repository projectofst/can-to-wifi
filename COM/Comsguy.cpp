#include "COM/Comsguy.h"


Comsguy::Comsguy(QObject *parent) : QObject(parent)
{
    AllPorts = new LogRec;
}

Comsguy::~Comsguy() {
    return;
}

void Comsguy::updateCOM(PortType type, QString target, QString secondtarget)
{
    switch (type) {
    case Start:
        CurrentCOM[target] = new SerialPort;
        open(target);
        break;
    case Close:
        if (secondtarget.compare("All")==0) {
            AllPorts->close();
        } else if (!CurrentCOM.empty()) {
            close(target);
        }
        break;
    case Wifi:
        CurrentCOM[target] = new UdpSocket;
        connect(this,SIGNAL(broadcast_arrival_of_CAN_message(CANdata)),
                CurrentCOM.values(target).at(CurrentCOM.values(target).size()-1),SLOT(send(CANdata)));
        break;
//    case LogReader:
//        CurrentCOM[target] = new LogPlay;
//        open(target);
//        break;
    case LogRecorder:
        if (secondtarget.compare("All")==0) {
            AllPorts->open(target);
            connect(this,SIGNAL(broadcast_arrival_of_CAN_message(CANdata)),AllPorts,SLOT(send(CANdata)));
        } else {
            CurrentCOM.insertMulti(secondtarget, new LogRec);
            CurrentCOM.values(secondtarget).at(0)->open(target);
            connect(CurrentCOM.values(secondtarget).at(1),SIGNAL(send_to_log(CANdata)),CurrentCOM.values(secondtarget).at(0),SLOT(send(CANdata)));
            emit Com_Status(CurrentCOM.keys());
        }
        break;
    case KvaserLog:
        CurrentCOM[target] = new SerialPort;
        open(target);
        break;
    case KvaserSerial:
        CurrentCOM[target] = new SerialPort;
        open(target);
        break;
    }
}

void Comsguy::send(CANdata msg)
{
    foreach (QString Key, CurrentCOM.uniqueKeys())
        CurrentCOM.values(Key).at(CurrentCOM.values(Key).size()-1)->send(msg);
}

void Comsguy::open(QString target){
    if (!CurrentCOM.contains(target)) {
        return;
    }
    if (CurrentCOM.value(target)->open(target)) {
        connect(CurrentCOM.values(target).at(CurrentCOM.values(target).size()-1), SIGNAL(new_messages(int)), this, SLOT(broadcast_messages(void)));
        this->close("(none)");
        emit Com_Status(CurrentCOM.keys());
    } else {
        printf("%s","Error! - Unable to open device port.");
    }
}

void Comsguy::close(QString target){
    if (target.startsWith("[LOGREC] ")) {
        target.replace(QString("[LOGREC] "), QString(""));
        CurrentCOM.values(target).at(0)->close();
        CurrentCOM.take(target);
    } else {
        foreach (COM *device, CurrentCOM.values(target))
            device->close();
            CurrentCOM.remove(target);
    }
    emit Com_Status(CurrentCOM.keys());
}

void Comsguy::broadcast_messages()
{
    foreach (QString Key, CurrentCOM.uniqueKeys()) {
        std::pair<int, CANdata> package;
        package = CurrentCOM.values(Key).at(CurrentCOM.values(Key).size()-1)->pop();
        emit broadcast_arrival_of_CAN_message(package.second);
    }
}
