#ifndef LOGREC_H
#define LOGREC_H

#include "COM/COM.h"

#include <QObject>
#include <QTimer>
#include <QFile>
#include <QTextStream>
#include <QString>

#define MAX_LOG_LINES		1000	// Number of messages to hold in window
#define LOG_FLUSH_timestamp	5000

namespace Ui {
    class LogRec;
}

class LogRec : public COM
{
    Q_OBJECT

public:
    LogRec();
    virtual bool open(const QString filename) override;
    virtual int  close(void) override;

public slots:
    virtual int send(CANdata msg) override;
    virtual void read(void) override;

private:
    QDateTime time;
    QTimer _logtimer;
    QFile _logfile;
    QTextStream _logstream;

private slots:
    void flush(void);

};

#endif // LOGREC_H
