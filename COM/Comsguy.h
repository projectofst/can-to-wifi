#ifndef COMSGUY_H
#define COMSGUY_H

#include <QObject>
#include <QTextStream>
#include <QFile>
#include <QString>
#include <QMap>

#include "COM/COM.h"
#include "COM/SerialPort.hpp"
#include "COM/LogRec.h"
#include "COM/UdpSocket.h"
#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/INTERFACE_CAN.h"

namespace Ui {
    class Comsguy;
}

typedef enum _PortType {
    Start = -1,
    Close = 0,
    Serial = 1,
    Wifi = 2,
    LogRecorder = 3,
    LogRecorderStatus = 4,
    LogReader = 5,
    KvaserLog = 6,
    KvaserSerial = 7,

} PortType;

class Comsguy : public QObject
{
    Q_OBJECT

public:
    explicit Comsguy(QObject *parent = nullptr);
    ~Comsguy(void);
    bool _linked;

public slots:
    void close(QString);
    void updateCOM(PortType,QString,QString);

private slots:
    void open(QString);
    void send(CANdata);
    //void write(QString string);
    void broadcast_messages();
    //void broadcast_raw_messages(void);

private:
    COM *Temp;
    LogRec *AllPorts;
    QMap<QString, COM*> CurrentCOM;
    bool LogRecStatus;

signals:
    void broadcast_arrival_of_CAN_message(CANdata);
    void Com_Status(QList<QString>);
    //void broadcast_arrival_of_raw_CAN_message(QByteArray);
};

#endif // COMSGUY_H
