QT -= gui
QT += network
QT += serialport

CONFIG += c++11 console
CONFIG += silent

QMAKE_CC = g++

QMAKE_CXXFLAGS += -std=c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
# DEFINES += QT_DEPRECATED_WARNINGS


SOURCES += \
    main.cpp \
    COM/Comsguy.cpp \
    COM/LogPlay.cpp \
    COM/LogRec.cpp \
    COM/SerialPort.cpp \
    COM/UdpSocket.cpp \
    can-ids/Devices/LOGGER_CAN.C \
    can-ids/Devices/AHRS_CAN.c \
    can-ids/Devices/ARM_CAN.c \
    can-ids/Devices/BMS_MASTER_CAN.c \
    can-ids/Devices/BMS_VERBOSE_CAN.c \
    can-ids/Devices/COMMON_CAN.c \
    can-ids/Devices/DASH_CAN.c \
    can-ids/Devices/DASH_STEER_CAN.c \
    can-ids/Devices/DCU_CAN.c \
    can-ids/Devices/GPS_CAN.c \
    can-ids/Devices/IIB_CAN.c \
    can-ids/Devices/INTERFACE_CAN.c \
    can-ids/Devices/SNIFFER_CAN.c \
    can-ids/Devices/STEER_CAN.c \
    can-ids/Devices/SUPOT_CAN.c \
    can-ids/Devices/TE_CAN.c \
    can-ids/CAN_IDs.c \
    can-ids/table.c \
    COM/COM.cpp

HEADERS += \
    COM/COM.h \
    COM/Comsguy.h \
    COM/LogPlay.h \
    COM/LogRec.h \
    COM/SerialPort.hpp \
    COM/UdpSocket.h \
    moc_predefs.h \
    can-ids/Devices/AHRS_CAN.h \
    can-ids/Devices/ARM_CAN.h \
    can-ids/Devices/BMS_MASTER_CAN.h \
    can-ids/Devices/BMS_VERBOSE_CAN.h \
    can-ids/Devices/COMMON_CAN.h \
    can-ids/Devices/DASH_CAN.h \
    can-ids/Devices/DASH_STEER_CAN.h \
    can-ids/Devices/DCU_CAN.h \
    can-ids/Devices/GPS_CAN.h \
    can-ids/Devices/IIB_CAN.h \
    can-ids/Devices/IMU_CAN.h \
    can-ids/Devices/INTERFACE_CAN.h \
    can-ids/Devices/LOGGER_CAN.h \
    can-ids/Devices/SNIFFER_CAN.h \
    can-ids/Devices/STEER_CAN.h \
    can-ids/Devices/SUPOT_CAN.h \
    can-ids/Devices/TE_CAN.h \
    can-ids/DOC/tablec.jinja \
    can-ids/CAN_IDs.h \
    can-ids/table.h \
    moc_predefs.h

DISTFILES += \
    can-ids/README.md \
    can-ids/table.txt \
    can-ids/DOC/docgen.py \
    can-ids/DOC/tablegen.py \
    can-ids/DOC/tables.py
