#include <stdbool.h>
#include <stdint.h>
#include "can-ids/CAN_IDs.h"
#include "TE_CAN.h"

void parse_te_main_message(uint16_t data[4], TE_MESSAGE *main_message)
{
	main_message->status.TE_status	= data [0];
	main_message->APPS				= data [1];
	main_message->BPS_pressure		= data [2];
	main_message->BPS_electric		= data [3];

	return;
}

void parse_te_debug_message(uint16_t data[4], TE_DEBUG_MESSAGE *debug_message)
{
	debug_message->APPS_0			= data [0];
	debug_message->APPS_1			= data [1];
	debug_message->BPS_pressure_0	= data [2];
	debug_message->BPS_pressure_1	= data [3];

	return;
}

void parse_te_raw_1_message(uint16_t data[4], TE_RAW_MESSAGE *raw_message){

	raw_message->apps_0 	= data[0];
	raw_message->apps_1 	= data[1];
	raw_message->pressure_0 = data[2];
	raw_message->pressure_1 = data[3];

	return;
}



void parse_te_raw_2_message(uint16_t data[4], TE_RAW_MESSAGE *raw_message){

	raw_message->electric = data[0];

	return;
}


void parse_can_te(CANdata message, TE_CAN_Data *data)
{
	if (message.dev_id != DEVICE_ID_TE) {
		return;
	}

	switch (message.msg_id) {
		case MSG_ID_TE_MAIN:
			parse_te_main_message(message.data, &(data->main_message));
			break;
		case MSG_ID_TE_DEBUG:
			parse_te_debug_message(message.data, &(data->debug_message));
			break;
		case MSG_ID_TE_RAW_1:
			parse_te_raw_1_message(message.data, &data->raw_message);
			break;
		case MSG_ID_TE_RAW_2:
			parse_te_raw_2_message(message.data, &data->raw_message);
			break;
	}

	return;
}
