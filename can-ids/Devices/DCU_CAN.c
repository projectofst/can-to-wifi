#include <stdbool.h>
#include <stdint.h>
#include "can-ids/CAN_IDs.h"
#include "DCU_CAN.h"

void parse_dcu_message_status(uint16_t data[4], DCU_MSG_Status *status)
{
    status->FB_BL            = data[0] >> 0  & 1;
    status->FB_CAN_E         = data[0] >> 1  & 1;
    status->FB_DRS           = data[0] >> 2  & 1;
    status->FB_CUB           = data[0] >> 3  & 1;
    status->FB_fans          = data[0] >> 4  & 1;
    status->FB_pumps         = data[0] >> 5  & 1;
    status->FB_EM            = data[0] >> 6  & 1;
    status->FB_SC_origin     = data[0] >> 7  & 1;
    status->FB_CUA           = data[0] >> 8  & 1;
    status->FB_TSAL          = data[0] >> 9  & 1;
    status->FB_fans_AMS      = data[0] >> 10 & 1;
    status->FB_AMS           = data[0] >> 11 & 1;
    status->SC_BSPD_to_bat   = data[0] >> 12 & 1;
    status->SC_MH_to_front   = data[0] >> 13 & 1;
    status->SC_front_to_BSPD = data[0] >> 14 & 1;

    status->CUA_sig          = data[1] >> 0  & 1;
    status->CUB_sig          = data[1] >> 1  & 1;
    status->fan_sig          = data[1] >> 2  & 1;
    status->SC_switch_sig    = data[1] >> 3  & 1;
    status->pump1_sig        = data[1] >> 4  & 1;
    status->pump2_sig        = data[1] >> 5  & 1;
    status->dcdc_switch_sig  = data[1] >> 6  & 1;
    status->BL_sig           = data[1] >> 7  & 1;
    status->buzz_sig         = data[1] >> 8  & 1;
    status->DRS_PWM          = data[1] >> 9  & 1;
    
    status->TEMP_PIC = data[2];
    status->TEMP_BOX = data[3];
}

void parse_dcu_message_current(uint16_t data[4], DCU_MSG_Current *current)
{
    current->LV_current = data[0];
    current->HV_current = data[1];
}

void parse_can_dcu(CANdata message, DCU_CAN_Data *data)
{
    if (message.dev_id != DEVICE_ID_DCU) {
        /*FIXME: send info for error logging*/
        return;
    }
    
    switch (message.msg_id) {
        case MSG_ID_DCU_STATUS:
            parse_dcu_message_status(message.data, &(data->status));
            break;
        case MSG_ID_DCU_CURRENT:
            parse_dcu_message_current(message.data, &(data->current));
            break;
    }
}
