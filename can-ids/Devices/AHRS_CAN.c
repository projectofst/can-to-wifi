#include "AHRS_CAN.h"

/* parse fuctions */

void parse_can_message_health_temperature(uint16_t data[4], AHRS_MSG_HEALTH_TEMPERATURE *health_temperature){

    int16_t temperature;

    health_temperature->health = 0b00111111 & data[0];
    temperature  = (int16_t) data[1] ;
    health_temperature->temperature_time = data[2];

    health_temperature->temperature = temperature /100;

    return;

}
void parse_can_message_gyro_z_accel_x(uint16_t data[4], AHRS_MSG_GYRO *gyro, AHRS_MSG_ACCEL *accel){

    int16_t gyro_z = 0;
    int16_t accel_x = 0;

    gyro_z = (int16_t) data[0];
    gyro->time_z  = data[1];
    accel_x = (int16_t) data[2];
    accel->time_x = data[3];

    gyro->z = gyro_z/100;
    accel->x = accel_x/10000;

    return;
}

void parse_can_message_gyro_x_y(uint16_t data[4], AHRS_MSG_GYRO *gyro){

    int16_t gyro_x = 0;
    int16_t gyro_y = 0;

    gyro_x = (int16_t) data[0];
    gyro_y = (int16_t) data[1];
    gyro->time_x_y = data[2];

    gyro->x = gyro_x/1000;
    gyro->y = gyro_y/1000;

    return;

}

void parse_can_message_accel_y_z(uint16_t data[4], AHRS_MSG_ACCEL *accel){

    int16_t accel_y = 0;
    int16_t accel_z = 0;

    accel_y = (int16_t) data[0];
    accel_z = (int16_t) data[1];
    accel->time_y_z = data[2];

    accel->y = accel_y/1000;
    accel->z = accel_z/1000;

    return;

}
void parse_can_message_mag(uint16_t data[4], AHRS_MSG_MAG *mag){

    int16_t mag_x;
    int16_t mag_y;
    int16_t mag_z;

    mag_x = (int16_t) data[0];
    mag_x = (int16_t) data[1];
    mag_z = (int16_t) data[2];
    mag->time = data[3];

    mag->x = mag_x/10;
    mag->y = mag_y/10;
    mag->z = mag_z/10;

    return;

}
void parse_can_message_quat(uint16_t data[4], AHRS_MSG_QUAT *quat){

    int16_t quat_A_B_aux;
    int16_t quat_C_D_aux;
    int8_t quat_A;
    int8_t quat_B;
    int8_t quat_C;
    int8_t quat_D;

    quat_A_B_aux = (int16_t) data[0];
    quat_C_D_aux = (int16_t) data[1];
    quat->time = data[2];

    quat_A = (quat_A_B_aux >>8) & 0x00FF;
    quat_B = quat_A_B_aux & 0x00FF;
    quat_C = (quat_C_D_aux >>8) & 0x00FF;
    quat_D = quat_C_D_aux & 0x00FF;

//for now stays like this... in the even of the number being too small arrangements will need to be done yto the data
    quat->A = (float)quat_A;
    quat->B = (float)quat_B;
    quat->C = (float)quat_C;
    quat->D = (float)quat_D;

    return;

}
void parse_can_message_euler_angle(uint16_t data[4], AHRS_MSG_EULER *euler){

    int16_t roll;
    int16_t pitch;
    int16_t yaw;

    roll = (int16_t) data[0];
    pitch = (int16_t) data[1];
    yaw = (int16_t) data [2];
    euler->time_angle = data[3];

    euler->roll = roll  /100;
    euler->pitch = pitch /100;
    euler->yaw = yaw /100;

    return;
}

void parse_can_message_euler_rate(uint16_t data[4], AHRS_MSG_EULER *euler){

    int16_t roll_rate;
    int16_t pitch_rate;
    int16_t yaw_rate;

    roll_rate = (int16_t) data[0];
    pitch_rate = (int16_t) data[1];
    yaw_rate = (int16_t) data[2];
    euler->time_rate = data[3];

    euler->roll_rate = roll_rate /10000;
    euler->pitch_rate = pitch_rate /10000;
    euler->yaw_rate = yaw_rate /100;

    return;
    
}


void parse_can_ahrs(CANdata message, AHRS_CAN_Data *data){
    if (message.dev_id != DEVICE_ID_AHRS) {
		/*FIXME: send info for error logging*/
        return;
	}  

    switch (message.msg_id){
		case MSG_ID_AHRS_HEALTH_TEMPERATURE:
            parse_can_message_health_temperature(message.data,&(data->health_temperature));
            break;
        case MSG_ID_AHRS_GYRO_Z_ACCEL_X:
            parse_can_message_gyro_z_accel_x(message.data, &(data->gyro), &(data->accel));
            break;
        case MSG_ID_AHRS_GYRO_X_Y:
            parse_can_message_gyro_x_y(message.data, &(data->gyro));
        case MSG_ID_AHRS_ACCEL_Y_Z:
            parse_can_message_accel_y_z(message.data, &(data->accel));
            break;
        case MSG_ID_AHRS_MAG:
            parse_can_message_mag(message.data, &(data->mag));
            break;
        case MSG_ID_AHRS_QUAT: 
            parse_can_message_quat(message.data, &(data->quat));
            break;
        case MSG_ID_AHRS_EULER_ANGLE:
            parse_can_message_euler_angle(message.data, &(data->euler));
            break;
        case MSG_ID_AHRS_EULER_RATE:
            parse_can_message_euler_rate(message.data, &(data->euler));
            break;
	}  
}