#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "can-ids/CAN_IDs.h"
#include "DASH_STEER_CAN.h"

#if 0
void parse_can_message_motor_off(uint16_t data[4],DASH_MSG_Motor *motor){
	motor->motor = data[0];
}

void parse_can_message_accumulator(uint16_t data[4], DASH_MSG_Accumulator *info) {
	info->max_temp = (data[0] >> 8) & 0xFF;
	info->mean_temp = data[0] & 0xFF;
    info->min_voltage = data[1];
	info->percentage = data[2];
}

void parse_can_message_peripherals(uint16_t data[4], DASH_MSG_Peripherals *peripherals){
	peripherals->pump2 = data[0] & 0b0001;
	peripherals->pump1 = (data[0] >>1) & 0b0001;
	peripherals->fans = (data[0] >>2) & 0b0001;
}

void parse_can_message_mode(uint16_t data[4], _MODES *mode){
	*mode = data[0];
}

void parse_can_message_tq_mode(uint16_t data[4], uint8_t *tq_mode){
	*tq_mode = data[0];
}
#endif
