#ifndef __DASH_CAN_H__
#define __DASH_CAN_H__

#include "can-ids/CAN_IDs.h"
#include <stdint.h>
#include <stdbool.h>

#define MSG_ID_DASH_STATUS	32

typedef struct {
	/*data [0]*/
	uint8_t board_temp;
	/*data [1]*/
	union {
		struct {
			bool AIR_plus: 1;
			bool AIR_minus: 1;
			bool IMD: 1;
			bool IMD_latch: 1;
			bool AMS: 1;
			bool RTD_mode: 1;
		};
		uint8_t LEDs :6;
	};
    bool SC;
	bool debug_mode;
} DASH_MSG_status;


// MAIN STRUCT
typedef struct {
	DASH_MSG_status status;
} DASH_CAN_Data;

void parse_can_message_status(uint16_t data[4], DASH_MSG_status *status);
void parse_can_dash(CANdata message, DASH_CAN_Data *data);

#endif
