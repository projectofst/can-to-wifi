#ifndef _GPS_CAN_H
#define _GPS_CAN_H

#include "../CAN_IDs.h"

#define MSG_ID_GPS_CLOCK_SYNC 32
#define MSG_ID_GPS_VELOCITY   33
#define MSG_ID_GPS_LATITUDE   34
#define MSG_ID_GPS_LONGITUDE  35

typedef struct{

    uint16_t miliseconds;

    uint16_t ground_speed;

    int16_t lat_dg;
    int16_t lat_min;
    int16_t lat_dec_min;

    int16_t long_dg;
    int16_t long_min;
    int16_t long_dec_min;

}GPS_PARSED_DATA;

void parse_can_gps                 (CANdata message, GPS_PARSED_DATA *data);
void parse_gps_message_clock       (CANdata message, GPS_PARSED_DATA *data);
void parse_gps_message_velocity    (CANdata message, GPS_PARSED_DATA *data);
void parse_gps_message_coordinates (CANdata message, GPS_PARSED_DATA *data); 



#endif