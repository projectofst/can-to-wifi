#ifndef _SUPOT_CAN_H
#define _SUPOT_CAN_H

#include "can-ids/CAN_IDs.h"

#include <stdint.h>

#define MSG_ID_SUPOT_POT 32

typedef struct{

    uint16_t front_left;
    uint16_t front_right;
    uint16_t rear_left;
    uint16_t rear_right;

}SUPOT_MSG_SIG;

void parse_can_supot_sig (CANdata message, SUPOT_MSG_SIG *supots_values);

#endif