#ifndef __TE_CAN_H__
#define __TE_CAN_H__

/* CAN Interface for Torque Encoder */

#include <stdbool.h>
#include <stdint.h>
#include "../CAN_IDs.h"

/* Message IDs */
#define MSG_ID_TE_DEBUG 32
#define MSG_ID_TE_RAW_1 33
#define MSG_ID_TE_RAW_2 34

/*Sub Structs*/
typedef struct {
	union {
		struct {
			bool CC_APPS_0: 1;
			bool CC_APPS_1: 1;
			bool CC_BPS_electric_0: 1;
			bool CC_BPS_pressure_0: 1;
			bool CC_BPS_pressure_1: 1;
			bool Overshoot_APPS_0: 1;
			bool Overshoot_APPS_1: 1;
			bool Overshoot_BPS_electric: 1;
			bool Overshoot_BPS_pressure_0: 1;
			bool Overshoot_BPS_pressure_1: 1;
			bool Implausibility_APPS: 1;
			bool Implausibility_APPS_Timer_Exceeded: 1;
			bool Implausibility_APPS_BPS: 1;		
			bool SC_Motor_Interlock_Front: 1;
            bool Debug_Mode: 1;
			bool Raw_Mode: 1;

		};
		uint16_t TE_status;
	};
} TE_CORE;

typedef enum{ 
    BRAKE_LOWER,
    BRAKE_UPPER,
    ACCELERATOR_LOWER,
    ACCELERATOR_UPPER

} INTERFACE_MSG_PEDAL_THRESHOLDS;

/*Message Structs*/
/*MSG_ID_TE_MAIN*/
typedef struct{

	TE_CORE status;

	uint16_t APPS;
	uint16_t BPS_pressure;
	uint16_t BPS_electric;

}TE_MESSAGE;

/*MSG_ID_TE_DEBUG*/
typedef struct{

	uint16_t APPS_0;
	uint16_t APPS_1;

	uint16_t BPS_pressure_0;
	uint16_t BPS_pressure_1;

}TE_DEBUG_MESSAGE;

typedef struct{

	uint16_t apps_0;
	uint16_t apps_1;
	uint16_t electric;
	uint16_t pressure_0;
	uint16_t pressure_1;

}TE_RAW_MESSAGE;


/*MAIN STRUCT*/
typedef struct{

	TE_MESSAGE main_message;
	TE_DEBUG_MESSAGE debug_message;
	TE_RAW_MESSAGE raw_message;

}TE_CAN_Data;

void parse_te_main_message (uint16_t data[4], TE_MESSAGE *main_message);
void parse_te_debug_message(uint16_t data[4], TE_DEBUG_MESSAGE *debug_message);
void parse_te_raw_1_message(uint16_t data[4], TE_RAW_MESSAGE *raw_message);
void parse_te_raw_2_message(uint16_t data[4], TE_RAW_MESSAGE *raw_message);

void parse_can_te(CANdata message, TE_CAN_Data *data);

#endif
