#include <stdbool.h>
#include <stdint.h>
#include "../CAN_IDs.h"
#include "IIB_CAN.h"

void parse_IIB_speed_message(uint16_t data[4], IIB_speed *speed){
	speed->FL = data[0];
	speed->FR = data[1];
	speed->RL = data[2];
	speed->RR = data[3];
}

void parse_IIB_error_message(uint16_t data[4], IIB_error *error){
	error->FL = data[0];
	error->FR = data[1];
	error->RL = data[2];
	error->RR = data[3];
}

void parse_IIB_values_message(uint16_t data[4], IIB_values *values){
	values->node                = (data[0] & 0b01100000000000000) >> 14;
	values->AMK_temp_motor      = (data[0] & 0b00011111111110000) >>4; 
	values->AMK_temp_inverter   = ((data[0] & 0b000000000001111) << 6) | ((data[1] & 0b01111110000000000) >> 10);
	values->AMK_temp_IGBT       = data[1] & 0b00000001111111111;
	values->torque              = data[2];
	values->magnetizing_current = data[3];
}

void parse_IIB_status_message(uint16_t data[2], IIB_status *status){

	status->INV_RTR = data[0] & 0b01; 
        status->CAR_RTR = (data[0] & 0b010) >> 1;
    	status->INV_ERROR_1 = (data[0] & 0b0100) >> 2;
    	status->INV_ERROR_2 = (data[0] & 0b01000) >> 3;
    	status->INV_ERROR_3 = (data[0] & 0b010000) >> 4;
    	status->INV_ERROR_4 = (data[0] & 0b0100000) >> 5;
    	status->INV_P_ERROR_1 = (data[0] & 0b01000000) >> 6;
    	status->INV_P_ERROR_2 = (data[0] & 0b010000000) >> 7;
    	status->INV_P_ERROR_3 = (data[0] & 0b0100000000) >> 8;
    	status->INV_P_ERROR_4 = (data[0] & 0b01000000000) >> 9;
    	status->SEND_ERROR = (data[0] & 0b010000000000) >> 10;
    	status->REGEN_OK = (data[0] & 0b0100000000000) >> 11;
    	status->TURNING_OFF = (data[0] & 0b01000000000000) >> 12;
    	status->TURNING_ON = (data[0] & 0b010000000000000) >> 13;
    	status->INV_HV = (data[0] & 0b0100000000000000) >> 14;
    	status->SDC1 = data[1] & 0b01;
    	status->SDC2 = (data[1] & 0b010) >> 1;
    	status->CONTROLLER = (data[1] &0b0100) >> 2;
    	status->I1 = (data[1] & 0b01000) >> 3;
    	status->I2 = (data[1] & 0b010000) >> 4;
    	status->I3 = (data[1] & 0b0100000) >> 5;
    	status->I4 = (data[1] & 0b01000000) >> 6;
		status->FRONT_OFF = (data[1] & 0b010000000) >> 7;
		status->REAR_OFF = (data[1] & 0b0100000000) >> 8;
}

void parse_IIB_lims_message(uint16_t data[4], IIB_lims *lims){
    lims->lim_rpm = data[0];
    lims->lim_torque_r = data[1];
    lims->lim_torque_f = data[2];
    
    return;
}

void parse_can_IIB(CANdata message, IIB_CAN_data *data){

	switch(message.msg_id){
		case MSG_ID_IIB_SPEED : 
			parse_IIB_speed_message(message.data, &(data->speed));
			return;
		case MSG_ID_IIB_ERROR :
			parse_IIB_error_message(message.data, &(data->error));
			return;
		case MSG_ID_IIB_VALUES :
			parse_IIB_values_message(message.data, &(data->values));
			return; 
	 	case MSG_ID_IIB_STATUS :
	 		parse_IIB_status_message(message.data, &(data->status));
			return;
		case MSG_ID_IIB_LIMS :
		    parse_IIB_lims_message(message.data, &(data->lims));
		    return;

		default : return;
	}
}
