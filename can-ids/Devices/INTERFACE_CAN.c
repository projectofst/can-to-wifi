#include <stdbool.h>
#include <stdint.h>
#include "can-ids/CAN_IDs.h"
#include "INTERFACE_CAN.h"

#include <stdio.h>


void parse_can_message_dcu_toggle(uint16_t data[4], INTERFACE_MSG_DCU_TOGGLE *dcu_toggle)
{
    dcu_toggle->code = (DCU_TOGGLE) (data[0] & 0b1111);
}

void parse_can_message_debug_toggle(uint16_t data0, INTERFACE_MSG_DEBUG_TOGGLE *debug_toggle)
{
    debug_toggle->device = (DEBUG_TOGGLE) (data0 & 0b1111);
}


void parse_can_message_pedal_thresholds (CANdata *message, INTERFACE_CAN_Data *data){

    data->pedal_thresholds = (INTERFACE_MSG_PEDAL_THRESHOLDS) message->data[0];

    return;
}

void parse_can_message_interface_inverters (uint16_t data[4], INTERFACE_MSG_INVERTERS *inverters){
   
    inverters->max_rpm = data[0];

    inverters->max_torque_rear = data[1];

    inverters->max_torque_front = data[2];

    inverters->reset = data[3];
}

void parse_can_message_interface_arm1 (CANdata msg, INTERFACE_MSG_ARM1 *arm1){
   
    arm1->mode = msg.data[0];

    arm1->gain = msg.data[1];

    arm1->tl = (msg.data[2] /100.0);

    arm1->power = msg.data[3];
}

void parse_can_message_set_steer_thresholds(CANdata message, INTERFACE_CAN_Data *data){

    data->steer_thresholds = (INTERFACE_MSG_STEER_THRESHOLDS) message.data[0];

}

void parse_can_message_interface_start_log(CANdata message, INTERFACE_CAN_Data *data){

    data->start_log.year = message.data[0] & 0x7FF;
    data->start_log.day  = message.data[0] >>11;
    data->start_log.month = message.data[1] & 0xF;
    data->start_log.hour = message.data[1] >> 4;
    data->start_log.minute = message.data[2] & 0x3F;
    data->start_log.second = message.data[2] >> 6;

    return;
}

void parse_can_message_interface_set_drs(CANdata message, INTERFACE_CAN_Data * data){

    data->set_drs.drs_sig = message.data[0];
    return;

}


void parse_can_interface(CANdata message, INTERFACE_CAN_Data *data){
	if (message.dev_id != DEVICE_ID_INTERFACE) {
		/*FIXME: send info for error logging*/
        return;
	}

	switch (message.msg_id) {
        case CMD_ID_INTERFACE_START_LOG:
            parse_can_message_interface_start_log(message, data);
            break;

        case CMD_ID_INTERFACE_DCU_TOGGLE:
			parse_can_message_dcu_toggle(message.data, &(data->dcu_toggle));
			break;
        case CMD_ID_INTERFACE_DEBUG_TOGGLE:
            parse_can_message_debug_toggle(message.data[0], &(data->debug_toggle));
            break;
        case CMD_ID_INTERFACE_SET_PEDAL_THRESHOLD:
            parse_can_message_pedal_thresholds(&message, data);
            break;
        case MSG_ID_INTERFACE_INVERTERS:
            parse_can_message_interface_inverters(message.data, &(data->inverters_limitation));
            break;
        case CMD_ID_INTERFACE_SET_STEER_THRESHOLD:
            parse_can_message_set_steer_thresholds(message, data);
            break;
        case MSG_ID_INTERFACE_ARM1:
            parse_can_message_interface_arm1(message, &(data->arm1));
            break;

	}
}
