from jinja2 import Template
import sys

devices = [f"{i}" for i in range(32)]
with open(sys.argv[1]) as can_ids:
    for line in can_ids.readlines():
        if '#define' in line:
            line = line.split()
            if len(line) < 3:
                continue
            if "DEVICE" in line[1]:
                devices[int(line[2])] = line[1][len("DEVICE_ID_"):]

with open(sys.argv[2]) as template:
    t = Template(template.read())
    h = t.render(placas=devices)

print(h)
            


