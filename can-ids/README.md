# **Readme**

Header unifying assigned device IDs and reserved message IDs.
- Every device ID and message ID must be updated [here](https://docs.google.com/spreadsheets/d/1InPFM-8DPZuTtoiYA0fSQX9e4EX1x2re0YPpqx2A7ws/edit?usp=sharing)



## **File sections**

**Device IDS**
Here are defined all the device IDs present in the car:
- every ID value label must follow the format DEVICE_ID_*DEVICE*
- Every DEVICE_ID_*DEVICE* is a 5 bit variable, hence the range of valid IDs is 1-31


**Message IDs**
-Every Message ID value label must follow the format MSG_ID_DEVICE_*DESC*, where *DESC* is an abbreviated description of the message contents.

**Reserved , High priority Message IDs (0-31)**
Here are defined all the reserved message IDs present in the car:
- Reserved message IDs are defined as a high priority, unique message ID, i.e, these IDs can't be defined anywhere else.
- The lower the message ID, the higher is its priority
- In this section you have device-specific IDs `(MSG_ID_DEVICE_*)` and common message ids `(MSG_ID_COMMON_*)`, in the common section. 

**/Devices Folder: Non-reserved, low priority message IDs (32-63)**
Every device in the can bus must have a `DEVICE_CAN.c` and `DEVICE_CAN.h` files in this folder:
- In these you must declare every non reserved message id, can message structs and can message parser for your device.
- Non-reserved message IDs are not unique amongst devices. 

# *Rules*

- All high priority message IDs (0-31) must be defined in the file CAN_IDs.h. Do not define them anywhere else
- All low priority message IDs (32-63) must be defined in the correspondant `DEVICE_CAN.h` file
- Do not repeat high priority message IDs.
- Do not use IDs without defining them
- If this readme is too confusing ask for help.