#include <QCoreApplication>

#include "COM/Comsguy.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Comsguy *comsguy = new Comsguy;

    QList<QSerialPortInfo> SerialList = QSerialPortInfo::availablePorts();
    QString serialport = argv[1];
    comsguy->updateCOM(Start, serialport,"");
    comsguy->updateCOM(Wifi, "Wifi", serialport);

    return a.exec();

}
